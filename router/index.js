import express from "express";

export const router = express.Router();

export default { router };

// Configurar primer ruta
router.get("/", (req, res) => {
  const params = {
    titulo: "Examen C2 - Tirado Díaz Jesús Javier",
    numDocente: req.query.numDocente,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    nivel: req.query.nivelAca,
    pagoPorHoraBase: req.query.pagoHr,
    horasImpartidas: req.query.hrImpartidas,
    numHijos: req.query.numHijos,
    isPost: false,

  };

  res.render("index", params);
});

router.post("/", (req, res) => {
  const { nivelAca, pagoHr, hrImpartidas, numHijos } = req.body;
  
  if(hrImpartidas <= 0 || pagoHr <= 0 || numHijos < 0 || nivelAca < 1 || nivelAca > 3){
    res.redirect('/');
  }else{
      const pagohrImpartidas = calcularPago(
        pagoHr,
        nivelAca,
        hrImpartidas
      ).toFixed(2);
      const impuesto = calcularImpuesto(pagohrImpartidas).toFixed(2);
      const bono = calcularBono(numHijos, pagohrImpartidas).toFixed(2);
    
      const totalAPagar = calcularTotalAPagar(pagohrImpartidas, impuesto, bono);
      const params = {
        titulo: "Examen C2 - Tirado Díaz Jesús Javier",
        pagohrImpartidas,
        impuesto,
        bono,
        totalAPagar,
        isPost: true,
      };
    
      res.render("index", params);
  }
});

function calcularPago(pagoHr, nivelAca, hrImpartidas) {
  let pagoBono = 0;
  pagoHr = pagoHr * 1;
  nivelAca = nivelAca * 1;
  pagoHr = pagoHr * 1;
  if (nivelAca == 1) {
    pagoBono = pagoHr + pagoHr * 0.3;
  } else if (nivelAca == 2) {
    pagoBono = pagoHr + pagoHr * 0.5;
  } else if (nivelAca == 3) {
    pagoBono = pagoHr * 2;
  }
  return pagoBono * hrImpartidas;
}

function calcularImpuesto(pagoTotal) {
  pagoTotal = pagoTotal * 1;
  return pagoTotal * 0.16;
}

function calcularBono(numHijos, pago) {
  numHijos = numHijos * 1;
  pago = pago * 1;
  if (numHijos == 0) {
    return pago * 0.05;
  } else if (numHijos == 1) {
    return pago * 0.1;
  } else if (numHijos == 2) {
    return pago * 0.2;
  } else {
    return 0;
  }
}

function calcularTotalAPagar(pagohrImpartidas, impuesto, bono){
  pagohrImpartidas = pagohrImpartidas * 1;
    impuesto = impuesto * 1;
    bono = bono * 1;
    return (pagohrImpartidas - impuesto + bono).toFixed(2);
}
